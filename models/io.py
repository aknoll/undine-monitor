#! /usr/bin/env python3
# -*- coding: utf-8 -*-

"""Undine-Monitor: I/O

    Input and ouput routines for the Undine-Monitor.

    Provides:
        get_latest_sql_data: <function> Retrieves the latest entry in the SQL
            pulseoximeter and spco2 tables.
        alarm: <function> Ring alarm in browser if necessary.
        periodic_frontend_update: <function> The core function for updating the
            frontend DIVs.
    """

###----------Imports---------------------------------------------------------###

import datetime
import mysql.connector
import pandas as pd

# Bokeh Imports.
from bokeh.io import curdoc

###----------Global variables------------------------------------------------###

DB_USERNAME = 'undine'
DB_PASSWORD = 'monitor'
DB_DATABASE = 'undine'

###----------Custom functions------------------------------------------------###

def get_latest_sql_data():
    '''Retrieve the latest entry from the SQL database.'''

    # Open database.
    database = mysql.connector.connect(
        host="localhost",
        user=DB_USERNAME,
        passwd=DB_PASSWORD,
        database=DB_DATABASE
    )

    # Get the latest entry in pulseoximetry table.
    pulseoxi_results = pd.read_sql_query(
        """SELECT * FROM pulseoximeter ORDER BY ID DESC LIMIT 1;""",
        database
    )

    # Get the latest entry in spco2 table.
    spco2_results = pd.read_sql_query(
        """SELECT * FROM spco2 ORDER BY ID DESC LIMIT 1;""",
        database
    )

    return pulseoxi_results, spco2_results

def alarm(alarm_flags):
    '''Play an alarm sound in browser.

    The alarm sound is played if triggered by the current entry in the SQL
    database. Quite simply, an empty DIV on the page is updated with an audio
    control.

    Args:
        alarm_flags: <boolean> Whether or not an alarm should be sounded.
    '''

    alarm_model = curdoc().get_model_by_name('ly_alarm').children[0]
    
    if 'red' in alarm_flags:
        alarm_model.text = """
            <audio controls autoplay="true">
            <source src="undine-monitor/static/alarm.mp3"
                    type="audio/mpeg">
              Your browser does not support the audio element.
            </audio>
        """
    else: alarm_model.text = ""

def periodic_frontend_update():
    '''Request latest serial data from SQL database and update the frontend.

    This function is the core of the undine monitor. It is periodically called
    (usually once every second) and updates the frontend web page with the
    latest surveillance data from the SQL database.

    '''

    # Query latest data from SQL database.
    pulseoxi_data, spco2_data = get_latest_sql_data()

    # Ring the alarm if necessary.
    # The alarm status is saved as colors; black means no alarm, red means alarm.
    # I honestly don't know why I chose to do that.
    alarm([pulseoxi_data.alarm.values[0], spco2_data.alarm.values[0]])

    # Update DIVS on page.
    status_elements = {
        'ly_pulseoxi_time': [
            (datetime.datetime.min + pulseoxi_data.time[0]).time(),
            pulseoxi_data.alarm
        ],
        'ly_pulseoxi_spo2': [pulseoxi_data.spo2[0], pulseoxi_data.alarm],
        'ly_pulseoxi_bpm': [pulseoxi_data.bpm[0], pulseoxi_data.alarm],
        'ly_spco2': [spco2_data.spco2[0], spco2_data.alarm]
    }

    for key, value in status_elements.items():
        curdoc().get_model_by_name(key).children[0].text = """
            <font size="60" color="{}">{}</font>
        """.format(
            value[1][0],
            value[0]
        )
