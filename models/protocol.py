#! /usr/bin/env python3
# -*- coding: utf-8 -*-

"""Undine-Monitor: Protocol output

    This module collects the data for one night upon user request, processes it
    and creates a odt document ready for download.

    Provides:

    """

###----------Imports---------------------------------------------------------###

import datetime

import mysql.connector
import pandas as pd

from odf import text, teletype
from odf.opendocument import load
from odf.style import Style, TextProperties

# Bokeh imports
from bokeh.io import curdoc

###----------Global variables------------------------------------------------###

DIV_START_TEXT = """<font size="60">Initialisierung...</font>"""
PATH_TO_TEMPLATE = """/var/www/undine-monitor/data/undine_template.odf"""

###----------Custom functions------------------------------------------------###

def odt_select_data():

    # Get the selected date from the input field.
    date_input = (curdoc().get_model_by_name('ly_date_input').children[0]).value

    db = mysql.connector.connect(
        host="localhost",
        user="undine",
        passwd="monitor",
        database='undine'
    )

    print('Select current index...')

    # Determine the last entry of the queried day.
    pulseoxi_latest_entry = pd.read_sql_query(
        """
        SELECT time, id FROM pulseoximeter
        WHERE (date = '{0}')
        ORDER BY id DESC LIMIT 1;
        """.format(date_input),
        db
    )

    print(pulseoxi_latest_entry)

    pulseoxi_latest_id = pulseoxi_latest_entry.id[0]

    spco2_latest_id = pd.read_sql_query(
        """
        SELECT id FROM spco2
        WHERE date = '{0}'
        AND time < '{1}'
        ORDER BY id DESC LIMIT 1;
        """.format(date_input, pulseoxi_latest_entry.time),
        db
    ).values[0][0]

    print('Select periodic SPO2 data...')

    pulseoxi_results = pd.read_sql_query(
        """
        SELECT *, TIMESTAMPDIFF(
          MINUTE,
          (LAG(CONCAT(date, ' ', time), 1) OVER (ORDER BY id)),
          CONCAT(date, ' ', time)
        ) AS timediff
        FROM pulseoximeter
        WHERE (
          (id BETWEEN {0}-172800 AND {0})
          AND (date = '{1}' or date = subdate('{1}', 1))
          AND (MINUTE(time) % 15 = 0)
          AND (SECOND(time) = 0)
          AND (alarm = 'black')
          AND NOT (spo2 = 'Keine Verbindung zu Masimo möglich.')
        )
        OR (
          (id BETWEEN {0}-172800 AND {0})
          AND alarm = 'red'
        );
        """.format(pulseoxi_latest_id, date_input),
        db
    )
    
    # Search for values from previous or next day. They can be discarded.
    cuts = pulseoxi_results.loc[
        pulseoxi_results.timediff > 300.0
    ][['date', 'time']]

    print('Select periodic SPCO2 data...')
    spco2_results = pd.read_sql_query(
        """
        SELECT *
        FROM spco2
        WHERE (
          (id BETWEEN {0}-172800 AND {0})
          AND (MINUTE(time) % 15 = 0)
          AND (SECOND(time) = 0)
          AND (alarm = 'black')
        )
        OR (
          (id BETWEEN {0}-172800 AND {0})
          AND alarm = 'red'
        )
        """.format(spco2_latest_id),
        db
    )

    print(pulseoxi_results.to_string())

    results = pd.merge(
        pulseoxi_results,
        spco2_results,
        how='inner',
        on=['date', 'time'],
        sort=True
    )

    print(results.to_string())

    '''
    print('Start lookup of missing values...')
    cursor = db.cursor()
    for idx, row in results_alarm.iterrows():
        print('Next lookup...')
        time = (datetime.datetime.min + row.time).time()

        cursor.execute("""
        SELECT spco2 from spco2
        WHERE  (id BETWEEN {0}-86400 AND {0})
        AND date = '{1}'
        AND time = '{2}';
        """.format(current_id, row.date, time))

        try:
            results_alarm.at[idx, 'spco2'] = cursor.fetchall()[0][0]
        except IndexError:
            results_alarm.at[idx, 'spco2'] = '----'

        cursor.execute("""
        SELECT spo2, bpm from pulseoximeter
        WHERE  (id BETWEEN {0}-86400 AND {0})
        AND date = '{1}'
        AND time = '{2}';
        """.format(current_id, row.date, time))
        try:
            res = cursor.fetchall()[0]
            results_alarm.at[idx, 'spo2'] = res[0]
            results_alarm.at[idx, 'bpm'] = res[1]
        except IndexError:
            results_alarm.at[idx, 'spo2'] = '---%'
            results_alarm.at[idx, 'bpm'] = '---'

        # Add new entry 5 minutes later.
        later_checkup = (datetime.datetime.min+row.time+datetime.timedelta(minutes=5)).time()

        cursor.execute("""
        SELECT * from spco2
        WHERE  (id BETWEEN {0}-86400 AND {0})
        AND date = '{1}'
        AND time = '{2}';
        """.format(current_id, row.date, later_checkup))

        later_checkup_results = pd.DataFrame(
            cursor.fetchall(),
            columns=['date', 'time', 'spco2', 'alarm_spco2', 'idx']
        )
        later_checkup_results['comment'] = 'Kontrolle nach Alarm.'

        cursor.execute("""
        SELECT spo2, bpm from pulseoximeter
        WHERE  (id BETWEEN {0}-86400 AND {0})
        AND date = '{1}'
        AND time = '{2}';
        """.format(current_id, row.date, later_checkup))

        try:
            res = cursor.fetchall()[0]
            later_checkup_results.at[0, 'spo2'] = res[0]
            later_checkup_results.at[0, 'bpm'] = res[1]
        except IndexError:
            results_alarm.at[0, 'spo2'] = '---%'
            results_alarm.at[0, 'bpm'] = '---'

        results_noalarm = results_noalarm.append(later_checkup_results, sort=True)
    '''
    #results = pd.merge(results_alarm, results_noalarm, how='outer', sort=True)

    results_previous_day = results.loc[
        results.date == cuts.date.values[0]
    ].loc[
        results.time >= cuts.time.values[0]
    ]
    
    results_current_day = results.loc[
        results.date > cuts.date.values[0]
    ]
    
    try:
        results_current_day = results_current_day.loc[
            results_current_day.time < cuts.time.values[1]
        ]
    except IndexError:
        results_current_day = results_current_day
        
    results = pd.merge(
        results_previous_day,
        results_current_day,
        how='outer',
        sort=True
    )
        
    return results

def print_odt():
    """Output data for one night in a PDF file."""

    data = odt_select_data()
    date_input = (curdoc().get_model_by_name('ly_date_input').children[0]).value.strftime('%d.%m.%y')
    textdoc = load(PATH_TO_TEMPLATE)

    # Find all text elements.
    texts = textdoc.getElementsByType(text.P)

    children = []
    for string in texts:
        old_text = teletype.extractText(string)
        if 'time' in old_text:

            idx = int(old_text.split('_')[1])-1
            try:
                timediff = data.iloc[idx]
                time = (datetime.datetime.min + timediff.time).time()
                new_text = str(time)
            except IndexError:
                new_text = ""

        elif 'so2' in old_text:
            idx = int(old_text.split('_')[1])-1
            try:
                new_text = str(data.iloc[idx].spo2)
                if new_text == 'nan':
                    new_text = '---%'
            except IndexError:
                new_text = ""

        elif 'hf' in old_text:
            idx = int(old_text.split('_')[1])-1
            try:
                new_text = str(data.iloc[idx].bpm)
                if new_text == 'nan':
                    new_text = '---'
            except IndexError:
                new_text = ""

        elif 'pco2' in old_text:
            idx = int(old_text.split('_')[1])-1
            try:
                new_text = str(data.iloc[idx].spco2)
                if new_text == 'nan':
                    new_text = '--'
                else: new_text = new_text[-2:]
            except IndexError:
                new_text = ""

        elif 'comment' in old_text:
            #idx = int(old_text.split('_')[1])-1
            #try:
            #    new_text = str(data.iloc[idx].comment)
            #except IndexError:
            #    new_text = ""
            new_text = ""

        elif 'date_here' in old_text:
            new_text = "{}".format(date_input)

        else: new_text = old_text

        new_S = text.P()

        new_S.setAttribute("stylename", string.getAttribute("stylename"))
        new_S.addText(new_text)

        children.append([new_S, string])

    for child in children:
        old_text = teletype.extractText(child[1])
        new_text = teletype.extractText(child[0])
        child[1].parentNode.appendChild(child[0])
        child[1].parentNode.removeChild(child[1])

    # Save new document.
    textdoc.save('/var/www/undine-monitor/data/protokoll.odt')

    # Trigger download
    text_now = curdoc().get_model_by_name('ly_status').children[0]
    if text_now.text == '1':
        text_now.text = '2'
    else: text_now.text = '1'

if __name__ == '__main__':
    odt_select_data()
