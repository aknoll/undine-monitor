#! /usr/bin/env python3
# -*- coding: utf-8 -*-

"""Vib Shifts applet including vibrational spectra.

    This code provides the main view for the vibrational shifts library with
    IR spectra, triggered upon clicking on one of the data points.

    Args:
        None

    Returns: Interactive Bokeh server application including vibrational spectra.
    """
import serial
import time
import mysql.connector

def read_pulseoximeter():
    '''Stream data from pulse oximeter into a pandas array.'''
    
    #Sample line: 11/27/19 16:04:38 SN=0000075996 SPO2=---% BPM=--- PI=--.--%
    #SPCO=--.-% SPMET=--.-% DESAT=-- PIDELTA=+-- ALARM=0000 EXC=000820\r\n

    #Open serial port of pulse oximeter.
    ser = serial.Serial('/dev/ttyUSB1', 9600, timeout=2)

    while True:
        pulseoximeter_null = {
            'date': time.strftime("%Y-%m-%d"),
            'time': time.strftime("%H:%M:%S"),
            'spo2': 'Keine Verbindung zu Masimo möglich.',
            'bpm': 'Keine Verbindung zu Masimo möglich.',
            'color': 'black'
        }
        
        line = ser.readline()
        try:
            data = line.decode("utf-8").split()
        except UnicodeDecodeError:
            data = ['No date', 'No time', 'SN=0000000000', 'SPO2=---%', 'BPM=---', 'PI=--.--%', 'SPCO=--.-%', 'SPMET=--.-%', 'DESAT=--', 'PIDELTA=+--', 'ALARM=1111', 'EXC=000808']

        try:
            # Detect alarm.
            color = 'black'
            if data[10][6:] != '0000':
                color = 'red'
            
            po_data = {
                'date': time.strftime("%Y-%m-%d"),
                'time': time.strftime("%H:%M:%S"),
                'spo2': data[3][5:],
                'bpm': data[4][4:],
                'color': color
            }

        except IndexError:
            po_data = pulseoximeter_null

        po_db = mysql.connector.connect(
            host="localhost",
            user="undine",
            passwd="monitor",
            database='undine'
        )

        mycursor = po_db.cursor()

        sql = "INSERT INTO pulseoximeter (date, time, spo2, bpm, alarm) VALUES (%s, %s, %s, %s, %s);"
        val = (po_data['date'], po_data['time'], po_data['spo2'], po_data['bpm'], po_data['color'])
        mycursor.execute(sql, val)

        po_db.commit()
        '''
        mycursor.execute("SELECT * FROM pulseoximeter")
        myresult = mycursor.fetchall()
        for x in myresult:
            print(x)
        '''
    ser.close()

    #if data.alarm.str.slice(start=6).values[0] != '0000':
    #    table_data.data['color'] = ['red']
    #    playsound('data/alarm.mp3')

if __name__ == '__main__':
    read_pulseoximeter()
