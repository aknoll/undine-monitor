#! /usr/bin/env python3
# -*- coding: utf-8 -*-

"""Vib Shifts applet including vibrational spectra.

    This code provides the main view for the vibrational shifts library with
    IR spectra, triggered upon clicking on one of the data points.

    Args:
        None

    Returns: Interactive Bokeh server application including vibrational spectra.
    """
import serial
import time
import mysql.connector

spco2_null = {
    'date': 'Keine Verbindung!',
    'time': 'Keine Verbindung!',
    'spco2': 'Keine Verbindung zu TINA möglich.',
    'color': ['red']
}

def read_spco2():
    '''Stream data from pulse oximeter into a pandas array.'''

    #Open serial port of pulse oximeter.
    ser = serial.Serial('/dev/ttyUSB0', 19200, timeout=2)

    while True:
        line = ser.readline()

        try:
            data = line.decode("utf-8").split(',')
        except UnicodeDecodeError:
            data = ['No time', '----', 'N', '---', 'N', '---', 'N', '---', '--.-', '---', '-----', 'R', 'N\r\n']

        # Detect alarm.
        color = 'black'
        if data[2] == 'A':
            color = 'red'

        try:
            spco2_data = {
                'date': time.strftime("%Y-%m-%d"),
                'time': time.strftime("%H:%M:%S"),
                'spco2': data[1],
                'color': color
            }

            print(data)

        except IndexError:
            spco2_data = spco2_null

        spco2_db = mysql.connector.connect(
            host="localhost",
            user="undine",
            passwd="monitor",
            database='undine'
        )

        mycursor = spco2_db.cursor()

        sql = "INSERT INTO spco2 (date, time, spco2, alarm) VALUES (%s, %s, %s, %s);"
        val = (spco2_data['date'],
               spco2_data['time'],
               spco2_data['spco2'],
               spco2_data['color'],
               )
        mycursor.execute(sql, val)

        spco2_db.commit()

    ser.close()

if __name__ == '__main__':
    read_spco2()
