#! /usr/bin/env python3
# -*- coding: utf-8 -*-

"""Undine-Monitor: Main Bokeh application.

The main Bokeh applet builder for the Undine-Monitor. This module compiles the
Bokeh widgets for the frontend, collects the latest serial data from the SQL
database and triggers the periodic update of the web page.

"""

###----------Imports---------------------------------------------------------###

import datetime

# Bokeh Imports.
from bokeh.layouts import layout
from bokeh.io import curdoc
from bokeh.models.widgets import Div, Button, DatePicker
from bokeh.models.callbacks import CustomJS

# Custom imports
from models import protocol
from models import io

###----------Global variables------------------------------------------------###

DIV_START_TEXT = """<font size="60">Initialisierung...</font>"""

###----------Specify layout--------------------------------------------------###

LAYOUT_DICT = {
    # Live content DIVs.
    'pulseoxi_spo2': Div(text=DIV_START_TEXT),
    'spco2': Div(text=DIV_START_TEXT),
    'pulseoxi_bpm': Div(text=DIV_START_TEXT),
    'pulseoxi_time': Div(text=DIV_START_TEXT),

    # Interactive buttons.
    'send_button': Button(
        label="PDF abschicken",
        button_type="success",
        visible=False
    ),

    # Date input field
    'date_input': DatePicker(
        value=datetime.date.today(),
        min_date='2020-03-23',
        max_date=datetime.datetime.now(),
        visible=False
    ),

    # Status filed
    'status': Div(text='Kein Datum gewählt.'),

    # Alarm audio control
    'alarm' : Div()
}

###----------Assign callbacks------------------------------------------------###

# User may query to print a protocol.
LAYOUT_DICT['send_button'].on_click(protocol.print_odt)

CALLBACK = CustomJS(
    code="""
    var link = document.createElement('a');
    link.href = '/data/protokoll.odt';
    link.download = 'protokoll.odt';
    link.dispatchEvent(new MouseEvent('click'));
    """
)

LAYOUT_DICT['status'].js_on_change('text', CALLBACK)

###----------Finalize layout-------------------------------------------------###

for key, value in LAYOUT_DICT.items():
    curdoc().add_root(
        layout(
            value,
            sizing_mode='stretch_both',
            name='ly_{}'.format(key)
        )
    )

curdoc().add_periodic_callback(io.periodic_frontend_update, 1000)
curdoc().title = 'Undine-Monitor'
