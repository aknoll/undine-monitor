#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias python=python3
wlan() {
    sudo killall wpa_supplicant
    sudo rm -r /run/wpa_supplicant/wlp61s0

    sudo wpa_supplicant -B -i wlan0 -c/etc/wpa_supplicant/"$1".conf
}


export PATH="$PATH:/home/pi/.local/bin"

PS1='[\u@\h \W]\$ '

sudo killall python3
nohup python3 -u /var/www/undine-monitor/read_po.py </dev/null >/dev/null 2>&1 &
nohup python3 -u /var/www/undine-monitor/read_spco2.py </dev/null >/dev/null 2>&1 &
sudo killall bokeh
/home/pi/.local/bin/bokeh serve --port 5100 --allow-websocket-origin raspberrypi /var/www/undine-monitor/ </dev/null >/dev/null 2>&1 &
sudo systemctl restart apache2
