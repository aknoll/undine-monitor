# SPO2 and Pulsoximetry monitor - Installation guide on Raspbian Buster Lite

This project is a guide including the necessary files for setting up an Apache
web server on a Raspberry Pi to display the data from a medical SPO2/ SPCO2 and pulseoximetry monitor.

## Operating system installation
* Download Rapsbian Buster Lite .zip file from Raspbian webpage. Unzip it.
* Copy .img file to unmounted SD card in FAT format:
```
dd bs=4M if=2019-09-26-raspbian-buster-lite.img of=/dev/sdX conv=fsync
```
* Run sync to flush the drive cache.
* Remove SD card and place it in the RaspberryPi.

## First steps on the RaspberryPi

* Connect to the internet using wpa_supplicant.
* Enable SSH in Raspberry. Type ```sudo raspi-config```, go to Interface Options
and enable SSH.
* Run ```apt-get update``` and ```apt-get upgrade```.
* Install python3-pip.
* Install numpy dependency libatlas-base-dev.
* Use pip to install pandas, pySerial.
* Install libjpeg-dev and zlib1g-dev .
* Use pip to install bokeh.
* Configure network in raspi-
* Put bokeh directive and sudo systemctl start apache2 in .bashrc
